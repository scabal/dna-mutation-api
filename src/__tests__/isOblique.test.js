var isOblique = require('../utils/mutation/isOblique'); 

describe('isOblique', () => {

    it('Valida cadena sin mutacion ["ATGCGA","ATGCGA"]', () => {
        expect(isOblique(["ATGCGA","ATGCGA"])).toBe(false);
    });

    it('Valida cadena sin mutacion ["ATGCGA","ATGCGA","ATGCGA","ATGCGA"]', () => {
        expect(isOblique(["ATGCGA","ATGCGA","ATGCGA","ATGCGA"])).toBe(false);
    });

    it('Valida cadena con mutacion ["ATGCGA","CAGTGC","TTATGT","AGAAGG"]', () => {
        expect(isOblique(["ATGCGA","CAGTGC","TTATGT","AGAAGG"])).toBe(true);
    });

    it('Valida cadena sin mutacion ["ATGCGA","CAGTGC","TTATGT","AGATAG"]', () => {
        expect(isOblique(["ATGCGA","CAGTGC","TTATGT","AGATAG"])).toBe(false);
    });

})