var isValid = require('../utils/mutation/isValid'); 

describe('isValid', () => {

    it('Valida caracter valida "A"', () => {
        expect(isValid('A')).toBe(true);
    });

    it('Valida caracter valida "T"', () => {
        expect(isValid('T')).toBe(true);
    });

    it('Valida caracter valida "C"', () => {
        expect(isValid('C')).toBe(true);
    });

    it('Valida caracter valida "G"', () => {
        expect(isValid('G')).toBe(true);
    });

    it('Valida caracter invalida "!"', () => {
        expect(isValid('!')).toBe(false);
    });

    it('Valida caracter invalida "X"', () => {
        expect(isValid('X')).toBe(false);
    });
    
    it('Valida string valida "ATGCGA"', () => {
        expect(isValid('ATGCGA')).toBe(true);
    });

    it('Valida string invalida "ATXCGA"', () => {
        expect(isValid('ATXCGA')).toBe(false);
    });

    it('Valida cadena invalida ["ATGCGA","ATXCGA"]', () => {
        expect(isValid(["ATGCGA","ATXCGA"])).toBe(false);
    });

})