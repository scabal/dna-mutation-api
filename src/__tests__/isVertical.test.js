var isVertical = require('../utils/mutation/isVertical'); 

describe('isVertical', () => {

    it('Valida cadena sin mutacion ["ATGCGA","ATGCGA"]', () => {
        expect(isVertical(["ATGCGA","ATGCGA"])).toBe(false);
    });

    it('Valida cadena con mutacion ["ATGCGA","ATGCGA","ATGCGA","ATGCGA"]', () => {
        expect(isVertical(["ATGCGA","ATGCGA","ATGCGA","ATGCGA"])).toBe(true);
    });

    it('Valida cadena con mutacion ["ATGCGA","CAGTGC","TTATGT","AGAAGG"]', () => {
        expect(isVertical(["ATGCGA","CAGTGC","TTATGT","AGAAGG"])).toBe(true);
    });

    it('Valida cadena sin mutacion ["ATGCGA","CAGTGC","TTATGT","AGAAAG"]', () => {
        expect(isVertical(["ATGCGA","CAGTGC","TTATGT","AGAAAG"])).toBe(false);
    });

})