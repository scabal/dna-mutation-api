const express = require("express");
const cors = require("cors");

const app = express();
require("dotenv").config();

// ORM config
const env = process.env.NODE_ENV || "development";
const config = require(__dirname + "/config/config.js")[env];
const API_VERSION = config.apiVersion;

// Load routings
// const userRoutes = require("./routes/user");
const loadRouting = require("./routes");

//Para rellenar el req.body
app.use(express.json({ limit: '10mb' }));
app.use(express.urlencoded({ limit: '10mb' }));
app.disable('x-powered-by');

// Configure Header HTTP
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method"
  );
  res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
  res.header("Allow", "GET, POST, OPTIONS, PUT, DELETE");
  next();
});
app.use(cors(/* corsOptions */));

// Router Basic
loadRouting.forEach((file) => {
  app.use(`/dna-api/${API_VERSION}`, file);
});

app.get('/', (req, res) => {
res.json({ Message: "Works!" });
});

module.exports = app;
