var express = require("express");
var JwtController = require("../controllers/jwt");

const api = express.Router();

//ruta, funcion del controlador a ejecutar en la ruta al hacer un post
api.get("/getToken", JwtController.getToken);

module.exports = api;