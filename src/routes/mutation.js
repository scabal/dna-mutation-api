var express = require("express");
var MutationController = require("../controllers/mutation");
var md_auth = require("../middlewares/authenticated");

const api = express.Router();

//ruta, funcion del controlador a ejecutar en la ruta al hacer un post
api.get("/tryApi", MutationController.tryApi); 

api.get("/stats",[md_auth.ensureAuth], MutationController.getMutations);
api.post("/mutation",/* [md_auth.ensureAuth], */ MutationController.addDNA);

module.exports = api;