const jwt = require("jwt-simple");

const SECRET_KEY = "gR7cH9Svfj8JLe4c18hheb3902nh5DsA";

exports.ensureAuth = (req, res, next) => {
    if(!req.headers.autorization) {
        return res
        .status(403)
        .send({ message: "La petición no tiene cabecera de Autenticación." });
    }

    const token = req.headers.autorization.replace(/['"]+/g, "");

    try {
        var payload = jwt.decode(token, SECRET_KEY);

    } catch (error) {
        console.log(error);
        return res.status(404).send({ message: "Token invalido." });
    }

    req.user = payload;
    next();
}