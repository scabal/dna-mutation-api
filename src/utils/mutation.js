var isHorizontal = require('./mutation/isHorizontal'); 
var isVertical = require('./mutation/isVertical'); 
var isOblique = require('./mutation/isOblique'); 

const hasMutation = (dna) => {

    let response = false;

    const resHorizontal = isHorizontal(dna);
    const resVertical = isVertical(dna);
    const resOblique = isOblique(dna);

    if(resHorizontal || resVertical || resOblique ){
        response = true
    }

    console.log((response ? "tiene mutacion" : "no tiene mutacion"));
    return response
}


module.exports = hasMutation;
