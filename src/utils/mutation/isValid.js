function invalidDNA(element, index, array) {
    
    var letters = /^[ATCG]+$/;
    const invChar = !letters.test(element);
    return invChar;
}

function isValid(dna) {

    let response
    if (typeof dna === 'string') {
        response = !invalidDNA(dna)
    } else {
        response = !dna.find(invalidDNA); // Itera elemento x elemento hasta que retorne true
    }
    // console.log(response + " is Valid");
    return response
}


module.exports = isValid;
