var { mutation } = require("../app/models/");

const tryApi = async (req, res) => {
    res.status(200).json({ message: "success test api" });
};

const getMutations = async (req, res) => {
    await mutation.findAll()
    .then((data) => {
        
        let count_mutations = data.filter(element => element.isMutated === true).length
        let count_no_mutations = data.filter(element => element.isMutated === false).length
        
        res.status(403).json({count_mutations, count_no_mutations,ratio: count_mutations/count_no_mutations});
    })
    .catch((error) => {
        console.log(error)
        res.status(403).send({
            code: "Error getting the consult",
        })
    });
};

const addDNA = async (req, res) => {
    const { dna } = req.body;
    var hasMutation = require("../utils/mutation")
    var isValid = require('../utils/mutation/isValid');
    
    const resIsValid = isValid(dna);

    if(!resIsValid){
        console.log("Los caracteres solo pueden ser: A, T, C, G");
        res.status(403).send({
            code: "Check the dna charge",
        })
    }else{
        let isMutated = hasMutation(dna);
        await mutation.create({
            isMutated,
          })
          .then((data) => {
              res.status(200).send({
                message: "Mutation added succefully",
              });
            })
          .catch((error) => {
              res.status(403).send({
                  code: "Error getting the consult",
              })
          });
    }
};


module.exports = {
    getMutations,
    addDNA,
    tryApi
};