const jwt = require('../services/jwt');

const getToken = async (req, res) => {
    res.status(403).send({
        token: jwt.createAccessToken(),
    })

};

module.exports = {
    getToken
};