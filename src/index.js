//Config app
const app = require("./app.js");
const { sequelize } = require("./app/models/index.js");
//set database enviroment
require("dotenv").config();

//Config database
const env = process.env.NODE_ENV || "development";
const config = require(__dirname + "/config/config.js")[env];

//Start server
const PORT = process.env.PORT || 3005;
app.listen(PORT, () => {
  console.log("#####################");
  console.log("###### API REST #####");
  console.log("#####################");
  console.log(
    `/console-api/${config.apiVersion}/`
  );
  console.log("Running on port: "+PORT);
  
  const testDbConnection = async () => {
    try {
      await sequelize.authenticate();
      /* sequelize.sync({ force: false }).then(() => {
        console.log("Connection success");
      }).catch(error => {
        console.log('Connection error', error);
      }); */
    } catch (err) {
      console.error("Unable to connect to the database:", err);
    }
  };

  testDbConnection();
});