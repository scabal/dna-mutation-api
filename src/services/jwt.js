const jwt = require("jwt-simple");
const moment = require("moment");

const SECRET_KEY = "gR7cH9Svfj8JLe4c18hheb3902nh5DsA";

exports.createAccessToken = function() {
    const playload = {
        createToken: moment().unix(),
    };

    return jwt.encode(playload, SECRET_KEY);
};
