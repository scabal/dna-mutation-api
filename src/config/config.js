const fs = require("fs");
require("dotenv").config();

module.exports = {
  development: {
    username: process.env.DEV_DB_USERNAME,
    password: process.env.DEV_DB_PASSWORD,
    database: process.env.DEV_DB_NAME,
    host: process.env.DEV_DB_HOSTNAME || "localhost",
    port: process.env.DEV_DB_PORT,
    dialect: process.env.DEV_DB_DIALECT,
    dialectOptions: {
      bigNumberStrings: true,
    },
    define: {
      //Generate foreign keys as user_id instead of userId
      uderscored: true,
    },
    apiVersion: process.env.DEV_API_VERSION || "v1",
  },
  test: {
    username: process.env.TEST_DB_USERNAME,
    password: process.env.TEST_DB_PASSWORD,
    database: process.env.TEST_DB_NAME,
    host: process.env.TEST_DB_HOSTNAME,
    port: process.env.TEST_DB_PORT,
    dialect: process.env.TEST_DB_DIALECT,
    dialectOptions: {
      bigNumberStrings: true,
    },
    define: {
      //Generate foreign keys as user_id instead of userId
      uderscored: true,
    },
    apiVersion: process.env.TEST_API_VERSION || "v1",
  },
  production: {
    username: process.env.PROD_DB_USERNAME,
    password: process.env.PROD_DB_PASSWORD,
    database: process.env.PROD_DB_NAME,
    host: process.env.PROD_DB_HOSTNAME,
    port: process.env.PROD_DB_PORT,
    dialect: process.env.PROD_DB_DIALECT,
    dialectOptions: {
      bigNumberStrings: true,
      socketPath: `/cloudsql/${process.env.INSTANCE_CONNECTION_NAME}`,
      /* ssl: {
        ca: fs.readFileSync(__dirname + '/mysql-ca-master.crt')
      } */
    },
    define: {
      //Generate foreign keys as user_id instead of userId
      uderscored: true,
    },
    apiVersion: process.env.PROD_API_VERSION || "v1",
  },
};
