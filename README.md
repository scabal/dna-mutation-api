# Mutacion de ADN

## Este programa valida si existe alguna mutacion en una secuencia de ADN

Recibe como parámetro un arreglo de cadena caracteres que representan cada fila de una tabla de NxN
con la secuencia del ADN. Las letras de los caracteres solo pueden ser: A, T, C, G; las cuales representan
cada base nitrogenada del ADN.

### Sin mutacion

```
[A][T][G][C][G][A]
[C][A][G][T][G][C]
[T][T][A][T][T][T]
[A][G][A][A][G][G]
[G][C][G][C][T][A]
[T][C][A][C][T][G]
```
### Con mutacion

```
[A][T][G][C][G][A]
[C][A][G][T][G][C]
[T][T][A][T][G][T]
[A][G][A][A][G][G]
[C][C][C][C][T][A]
[T][C][A][C][T][G]
```

Sabrás si existe una mutación si se encuentra más de una secuencia de cuatro letras iguales, de forma oblicua,
horizontal o vertical.

Ejemplo de caso con mutación:

```javascript
let dna = ["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA","TCACTG"];
```

---
# Instalación en local

Clonar el repositorio

instalar dependencias
```
npm i
```

Crear la base de datos "por defecto se llamara: adn"
```
npm run db-create
```
crear la tabla "mutations"
```
npm run db-migrate
```

Opcional: agregar datos a la tabla
```
npm run db-seed
```

---
# Realizar pruebas
```
npm run test
```
---
# Uso

Iniciar el servicio

```
npm start
```
Una vez iniciado el servidor web tendras a tu disposicion 
3 puntos de acceso

Alimentar la tabla
```
POST dna-api/v1/mutation
{
"dna":["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"]
}

```
resultado
- exito: {status:200,message:Mutation added succefully}
- error: {status:403,message:Error getting the consult}

---

Obtener token:
Para poder realizar la consulta de status, vas a necesitar introducir 
un token de autenticacion que valida que tienes permiso para utilizar este endpoint
por tal motivo, se facilita el siguiente punto de acceso "dna-api/v1/getToken"
el cual te entregara el token que vas a necesitar para poder consultar el status de mutaciones de adn
```
GET dna-api/v1/getToken
```
- resultado: token

---

Verificar status *Protegido*
```
GET dna-api/v1/stats,
Headers{
    Autorization: token
}
```
resultado
- exito: {status:403, count_mutations: value, count_no_mutations: value, ratio: prom}
- falla: {status:403,code: "Error getting the consult"}

